<?php

namespace Modules\Services\Models;

use Illuminate\Database\Eloquent\Model;

class Country extends Model
{

    public $incrementing = false;
    public $timestamps = false;
}