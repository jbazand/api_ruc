<?php

namespace Modules\Services\Helpers\Dni;

use Modules\Services\Utilities\Functions;
use Modules\Services\Utilities\Models\Person;
use GuzzleHttp\Client;

class Jne
{
    public static function search($number)
    {
        if (strlen($number) !== 8) {
            return [
                'success' => false,
                'message' => 'DNI tiene 8 digitos.'
            ];
        }

        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL,"http://aplicaciones007.jne.gob.pe/srop_publico/Consulta/Afiliado/GetNombresCiudadano?DNI={$number}");        
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        $response = curl_exec ($ch);         
        curl_close ($ch);
 

        if ($response) {
            $text = $response;
            $parts = explode('|', $text);
            if (count($parts) === 3) {
                $person = new Person();
                $person->numero = $number;
                $person->codigo_verificacion = Functions::verificationCode($number);
                $person->nombres_apellidos = $parts[0].' '.$parts[1].', '.$parts[2];
                $person->primer_apellido = $parts[0];
                $person->segundo_apellido = $parts[1];
                $person->nombres = $parts[2];

                return [
                    'success' => true,
                    'data' => $person
                ];
            } else {
                return [
                    'success' => false,
                    'message' => 'Datos no encontrados.'
                ];
            }
        }

        return [
            'success' => false,
            'message' => 'Conexión fallida.'
        ];
    }
}